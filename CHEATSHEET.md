# Cheatsheet

Quick references to common Handbrake settings.

## Usage example

```shell
docker run --rm -v $(pwd):/files letompouce/handbrake \
                -v /etc/localtime:/etc/localtime:ro         \
    --preset "H.265 MKV 2160p60" \
    -i 20190504_074035A.mp4 -o 20190504_074035A-H.265_MKV_2160p60.mp4
```

## Resize

Set a specific width, keep aspect ratio:

`--maxWidth 1080 --loose-anamorphic`

## Timelapse processing

We don't need to deal with audio:

`--audio none`
